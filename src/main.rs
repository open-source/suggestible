// SPDX-FileCopyrightText: 2024 Phil Höfer <phil@suma-ev.de>
// SPDX-License-Identifier: AGPL-3.0-only

use core::{str, time};
use std::collections::HashMap;
use std::error::Error;
use std::fs;
use std::fs::File;
use std::io::{self, BufRead, BufReader};
use std::str::FromStr;
use std::time::SystemTime;

use importers::file::FileImporter;
use importers::Importer;
use predictors::basic_set::SetPredictor;
use predictors::benchmark::BenchmarkingPredictor;
use predictors::composite::CompositePredictor;
use toml::Table;

use csv::ReaderBuilder;

use tiny_http::{Response, Server};

mod importers;

mod predictors;
use predictors::basic_markov::MarkovChain;
use predictors::{basic_markov, basic_set, composite, Predictor};

use postgres::{Client, NoTls};
use url::Url;

use prometheus::{Counter, Encoder, Histogram, HistogramOpts, Opts, Registry, TextEncoder};

fn main() -> Result<(), io::Error> {
    let mut last_update = std::time::SystemTime::now();
    let mut last_decay = std::time::SystemTime::now();
    let config_toml = read_config("data/config.toml")
        .or(read_config("config.toml"))
        .unwrap()
        .parse::<Table>()
        .unwrap();
    let mut config = HashMap::new();
    for (key, value) in config_toml {
        match value {
            toml::Value::String(s) => {
                config.insert(key, s);
            }
            toml::Value::Integer(i) => {
                config.insert(key, i.to_string());
            }
            _ => {
                // Ignore other types
            }
        }
    }


    let metrics = Registry::new();

    let total_requests_opts = Opts::new("suggestible_request_count", "suggestible");
    let total_requests = Counter::with_opts(total_requests_opts).unwrap();
    metrics.register(Box::new(total_requests.clone())).unwrap();

    let mut raw_predictor = CompositePredictor::new_from_config(config.clone(),Some(&metrics));
    let mut my_predictor = BenchmarkingPredictor::new_from_config(raw_predictor, config.clone(), Some(&metrics));

    for path in vec!["data/data.csv", "data.csv", "data_full.csv"].into_iter() {
        match &mut FileImporter::new_from_path(path) {
            Ok(imp) => {
                match my_predictor.update_from_importer(imp, 10000) {
                    Ok(count) => {
                        println!("Read {} queries from file {}.", count, path);
                    }
                    Err(e) => {
                        println!("Error while reading from file: {}", e)
                    }
                };
            }
            _ => {}
        }
    }
    // data locations: vec!["../../data/data.csv","data/data.csv","data.csv","data_full.csv"]

    // Test the function
    let word = "wie"; // replace with the word you want to check
    let top_words = &my_predictor.predict(&word, 3);
    println!("Example prediction for '{}': {:?}", word, top_words);

    let server = Server::http("0.0.0.0:8000").unwrap();
    let mut db_imp =
        importers::postgres::PostgresImporter::new_from_config(config.clone()).unwrap();


    
    
    loop {
        let request = server.recv_timeout(std::time::Duration::from_secs(1));
        match request {
            Ok(Some(r)) => {
                process_request(r, config.clone(), &mut my_predictor, &metrics);
                total_requests.inc();
            }
            _ => {}
        }

        let now = std::time::SystemTime::now();
        let decay_elapsed = now.duration_since(last_decay).expect("Time went backwards");
        let update_elapsed = now
            .duration_since(last_update)
            .expect("Time went backwards");

        if my_predictor.update_count() < 1000000
            && update_elapsed >= std::time::Duration::from_secs(10)
        {
            println!(
                "{} queries in db, importing more...",
                my_predictor.update_count()
            );

            if my_predictor.update_count() < 10000 {
                my_predictor.decay();
            }
            my_predictor.update_from_importer(&mut db_imp, 1000);
            last_decay = now;
            last_update = now;
        }
        if decay_elapsed >= std::time::Duration::from_secs(2 * 24 * 60 * 60) {
            //every 48h
            my_predictor.decay();
            last_decay = now;
        }
        if update_elapsed >= std::time::Duration::from_secs(2 * 60 * 60) {
            //every 2h
            my_predictor.update_from_importer(&mut db_imp, 1000);
            last_update = now;
        }
    }

    Ok(())
}

fn process_request(
    request: tiny_http::Request,
    config: HashMap<String, String>,
    markov_chain: &mut BenchmarkingPredictor<CompositePredictor>,
    metrics: &Registry
) {
    //   println!("received request! method: {:?}, url: {:?}, headers: {:?}",
    //           request.method(),
    //           request.url(),
    //           request.headers()
    //       );

    match Url::parse(
        (String::from("https://example.org") + (request.url().to_string().as_str())).as_str(),
    ) {
        Ok(url) => {
            match url.path() {
                "/metrics" => {
                    // Prometheus Test Implementation

                    // Gather the metrics.
                    let mut buffer = vec![];
                    let encoder = TextEncoder::new();
                    let metric_families = metrics.gather();
                    encoder.encode(&metric_families, &mut buffer).unwrap();

                    request.respond(Response::from_string(String::from_utf8(buffer).unwrap()));
                }
                "/healthz" => {
                    request.respond(Response::from_string(""));
                }
                "/suggest" => {
                    let query = get_query(request.url());
                    //println!("got query:{}", query.clone().unwrap());
                    match query {
                        Ok(query) => {
                            match config.get("auth") {
                                Some(server_auth) => match get_authkey(request.url().clone()) {
                                    Ok(client_auth) => {
                                        if client_auth != server_auth.clone() && query.len() > 0 {
                                            println!(
                                                "invalid auth:{}, server auth: {}, query: {}",
                                                client_auth, server_auth, query
                                            );
                                            request.respond(Response::from_string(""));
                                            return;
                                        }
                                    }
                                    _ => {}
                                },
                                _ => {}
                            }

                            let predict_count = match config.get("max_predict_count") {
                                Some(n) if n.parse::<usize>().unwrap_or_default() > 0 => {
                                    n.parse::<usize>().unwrap_or(5)
                                }
                                _ => 5,
                            };
                            if (query.len() < 2) {
                                request.respond(Response::from_string(""));
                                return;
                            }
                            let predictions = &markov_chain.predict(&query, predict_count);
                            //println!("Query: {}, Prediction:{}", query, prediction);
                            let response = Response::from_string(format!(
                                "[\"{}\",[{}]]",
                                query,
                                predictions.join(",")
                            ));
                            request.respond(response);
                        }
                        Err(e) => {
                            //println!("Error: {}",e);
                        }
                    }
                }
                _ => {
                    println!("unhandled path: {}", url.path());
                }
            }
        }
        Err(e) => {
            println!("parse error: {}", e);
        }
        _ => {
            //Error parsing URL
        }
    }
}

fn read_from_db(config: HashMap<String, String>, predictor: &mut CompositePredictor) {
    println!("starting import from db");
    let mut raw_list = String::new();
    let blocklist: Vec<&str> = match config.get("blocklist") {
        Some(list) => match fs::read_to_string(list) {
            Ok(l) => {
                raw_list = l.clone();
                raw_list.lines().collect()
            }
            _ => Vec::new(),
        },
        _ => Vec::new(),
    };

    let default_password = &String::from("");
    match (config.get("db_host"), config.get("db_password")) {
        (Some(db_host), Some(db_password)) => {
            let mut client = Client::connect(
                format!(
                    "host={} user=metager password={} dbname=postgres",
                    db_host, db_password
                )
                .as_str(),
                NoTls,
            );
            match client {
                Ok(mut c) => {
                    let mut count = 0;
                    match c.query(
                        "SELECT query FROM public.logs_partitioned ORDER BY time DESC LIMIT 1000",
                        &[],
                    ) {
                        Ok(rows) => {
                            for row in rows {
                                let query: &str = row.get(0);
                                predictor.update_from_query(query, Some(&blocklist));
                                count += 1;
                            }
                            println!("{} queries read from DB", count);
                        }
                        Err(e) => {
                            println!("Error while reading from DB: {}", e);
                        }
                    }
                }
                Err(e) => {
                    println!("Error while connecting to DB: {}", e);
                }
            }
        }
        _ => {
            println!("No DB config found. Skipping...");
        }
    }
}

fn read_config(file_path: &str) -> Result<String, io::Error> {
    fs::read_to_string(file_path)
}

//TODO: unify get_X() functions
fn get_query(request_url: &str) -> Result<String, url::ParseError> {
    let parsed_url = request_url
        .split_once('?')
        .map_or(request_url, |(_, after)| after);
    //println!("parsed_url:{}", parsed_url);
    let query_pairs = url::form_urlencoded::parse(parsed_url.as_bytes());
    for (key, value) in query_pairs {
        //println!("key:{}, value: {}", key, value);
        if key == "q" {
            return Ok(value.into_owned());
        }
    }
    Ok(String::from_str("").unwrap())
}
fn get_authkey(request_url: &str) -> Result<String, url::ParseError> {
    let parsed_url = request_url
        .split_once('?')
        .map_or(request_url, |(_, after)| after);
    //println!("parsed_url:{}", parsed_url);
    let query_pairs = url::form_urlencoded::parse(parsed_url.as_bytes());
    for (key, value) in query_pairs {
        //println!("key:{}, value: {}", key, value);
        if key == "auth" {
            return Ok(value.into_owned());
        }
    }
    Ok(String::from_str("").unwrap())
}

fn filter_markov_chain(markov_chain: &MarkovChain, min_count: usize) -> MarkovChain {
    let mut filtered_chain: MarkovChain = HashMap::new();

    for (key, followers) in markov_chain {
        let filtered_followers: HashMap<String, usize> = followers
            .iter()
            .filter(|&(_, &count)| count >= min_count)
            .map(|(word, &count)| (word.clone(), count))
            .collect();

        if !filtered_followers.is_empty() {
            filtered_chain.insert(key.clone(), filtered_followers);
        }
    }

    filtered_chain
}
