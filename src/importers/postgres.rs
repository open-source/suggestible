

use std::{collections::HashMap, fs::File};

use csv::ReaderBuilder;
use postgres::{Client, NoTls};

use super::{Importer,SearchQuery};

pub struct PostgresImporter {
    config: HashMap<String, String>,
    newest_entry: String
}

impl PostgresImporter {
    pub fn new_from_config(config: HashMap<String, String>) -> Result<PostgresImporter,std::io::Error> {

        Ok(PostgresImporter {
            config,
            newest_entry: String::from("2024-01-01 08:00:00.000 +0200") // default start date for reads
        })
    }
}

impl Importer for PostgresImporter {
    fn fetch_queries(&mut self, n: usize) -> Result<Vec<SearchQuery>, String> {

        let mut entries: Vec<SearchQuery> = Vec::new();
        let default_password = &String::from("");
        match (self.config.get("db_host"), self.config.get("db_password")) {
            (Some(db_host), Some(db_password)) => {
                let mut client = Client::connect(format!("host={} user=metager password={} dbname=postgres",db_host, db_password).as_str(), NoTls);
                match client {
                    Ok(mut c) => {
                        let mut count = 0;
                        match c.query(format!("SELECT query, TO_CHAR(time, 'YYYY-MM-DD HH24:MI:SS') FROM public.logs_partitioned WHERE time > '{}' ORDER BY time ASC LIMIT {}",self.newest_entry, n).as_str(), &[]) {
                            Ok(rows) => {
                                for row in rows {
                                    let query = SearchQuery {query: row.get(0)};
                                    self.newest_entry = row.get(1);
                                    entries.push(query);
                                    count += 1;
                                }
                                println!("{} queries read from DB. Newest query: {}", count, self.newest_entry);
                                
                            },
                            Err(e) => {
                                println!("Error while reading from DB: {}", e);
                                
                            }
                        }
                    },
                    Err(e) => {
                        println!("Error while connecting to DB: {}", e);
                        
                    }
                }
            },
            _ => {
                println!("No DB config found. Skipping...");
                
            }
        }  
            


        
        if entries.is_empty() {
            Err(String::from("Requested number of entries exceeds available data"))
        } else {
            Ok(entries)
        }
    }
}