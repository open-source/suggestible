

pub mod file;
pub mod postgres;

#[derive(Debug)]
pub struct SearchQuery {
    pub query: String,
}

pub trait Importer {
    fn fetch_queries(&mut self, n: usize) -> Result<Vec<SearchQuery>, String>;
}
