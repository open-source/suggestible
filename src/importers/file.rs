

use std::fs::File;

use csv::ReaderBuilder;

use super::{Importer,SearchQuery};

pub struct FileImporter {
    reader: csv::Reader<std::fs::File>
}

impl FileImporter {
    pub fn new_from_path(path:&str) -> Result<FileImporter,std::io::Error> {
        let file = File::open(path)?;
        let mut reader = ReaderBuilder::new().from_reader(file);

        Ok(FileImporter {reader})
    }
}

impl Importer for FileImporter {
    fn fetch_queries(&mut self, n: usize) -> Result<Vec<SearchQuery>, String> {

        let mut entries: Vec<SearchQuery> = Vec::new();
        for _ in 0..n {
            let mut record = csv::StringRecord::new();
            match self.reader.read_record(&mut record) {
                Ok(_) => {
                    if let Some(query) = record.get(5) {
                        entries.push(SearchQuery{query: query.to_string()});
                    }
                },
                _ => {}
            }
            
        }


        
        if entries.is_empty() {
            Err(String::from("Requested number of entries exceeds available data"))
        } else {
            Ok(entries)
        }
    }
}