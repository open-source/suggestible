use std::{collections::HashMap, f32::consts::E, fs::{self, File}};

use crate::importers::Importer;

use csv::ReaderBuilder;
use prometheus::{Counter, Gauge, Histogram, HistogramOpts, Opts, Registry};

use super::Predictor;

pub struct SetPredictor {
    set: HashMap<String,usize>,
    configuration: HashMap<String, String>,
    update_count: Counter,
    total_weight: Gauge
}

impl SetPredictor {
    pub fn new(metrics: Option<&Registry>) -> Self
    {
        let update_count_opts = Opts::new("suggestible_predictor_basic_set_update_count", "suggestible");
        let update_count = Counter::with_opts(update_count_opts).unwrap();
        let total_weight_opts = Opts::new("suggestible_predictor_total_weight", "suggestible");
        let total_weight = Gauge::with_opts(total_weight_opts).unwrap();
        
        SetPredictor {
            set: HashMap::new(),
            configuration: HashMap::new(),
            update_count: update_count,
            total_weight: total_weight
        }
    }

    pub fn new_from_config(config: HashMap<String, impl Into<String>>, metrics: Option<&Registry>) -> Self where Self:Sized {
        let mut configuration = HashMap::new();
        for (key, value) in config {
            configuration.insert(key, value.into());
        }


        let update_count_opts = Opts::new("suggestible_predictor_basic_set_update_count", "suggestible");
        let update_count = Counter::with_opts(update_count_opts).unwrap();
        let total_weight_opts = Opts::new("suggestible_predictor_basic_set_total_weight", "suggestible");
        let total_weight = Gauge::with_opts(total_weight_opts).unwrap();
        

        match metrics {
            Some(registry) => {
                // Register metrics
                registry.register(Box::new(update_count.clone())).unwrap();
                registry.register(Box::new(total_weight.clone())).unwrap();
            },
            None => {}
        }


        SetPredictor {
            set: HashMap::new(),
            configuration,
            update_count: update_count,
            total_weight: total_weight
        }
    }


}

impl Predictor for SetPredictor {
    
    fn update_count(&self) -> usize {
        return self.update_count.get() as usize;
    }

    fn update_from_importer<I: Importer>(&mut self, importer:  &mut I, count: usize) -> Result<usize,Box<dyn std::error::Error>> {

        let mut raw_list = String::new();
        let blocklist: Vec<&str> = match self.configuration.get("blocklist") {
            Some(list) => {
                match fs::read_to_string(list) {
                    Ok(l) => {
                        raw_list = l.clone();
                        raw_list.lines().collect()
                    },
                    _ => Vec::new()
                }
            },
            _ => Vec::new()
            
        };

        let data = importer.fetch_queries(count)?;
        let mut count = 0;

        for q in data.iter() {
            self.update_from_query(&q.query.as_str(), Some(&blocklist));
            count += 1;
        }
        Ok(count)
    }

    fn update_from_query(&mut self, query: &str, blocklist: Option<&Vec<&str>>) -> Result<(), Box<dyn std::error::Error>> {

        match blocklist {
            Some(b) => {
                for word in b.clone() {
                    if word.trim().len() > 1 && query.to_lowercase().contains(String::from(word).to_lowercase().as_str()) {
                        return Ok(());
                    }
                }
            },
            _ => {}
        }

        //println!("blocklist:{:?}",self.configuration);

        let lowercase_query: String = query.to_lowercase().chars()
        .map(|c| {
            if c.is_alphanumeric() {
                c
            } else {
                ' '
            }
        })
        .collect();
        let words = lowercase_query.split_whitespace();

        for word in words {
            let counter = self.set.entry(word.to_string()).or_insert(0);
            *counter += 1;
            self.total_weight.inc();
        }
        self.update_count.inc();

        Ok(())
    }

    fn total_weight(&self) -> usize {
        self.total_weight.get() as usize
    }

    fn decay(&mut self) -> () {


        //println!("total weight in SetPredictor before decay: {}",self.total_weight);
        let keys_to_remove: Vec<String> = self.set.iter_mut()
        .filter_map(|(key, value)| {
            if *value > 0 {
                *value -= 1;
                self.total_weight.dec();
                if *value < 1 {
                    Some(key.clone())
                } else {
                    None
                }
            } else {
                Some(key.clone())
            }
        })
        .collect();
        
        //println!("total weight in SetPredictor after decay: {}",self.total_weight);
        println!("removing {} uncommon terms of {} terms total", keys_to_remove.len(), self.set.len());
        for key in keys_to_remove {
            self.set.remove(&key);
        }
        
    }

    fn predict(&self, query: &str, n: usize) -> Vec<String> {
        let tf_threshold_conf = self.configuration.get("term_frequency_threshold").unwrap_or(&String::from("2")).parse::<usize>().unwrap();
        let tf_threshold = std::cmp::max(tf_threshold_conf, tf_threshold_conf*(self.update_count.get()as usize/1000));
if let Some(top_words) =
            get_top_completions(
                &self,
                query.split_whitespace().last().unwrap_or(""),
                n,
                tf_threshold
            )
        {
            let query_prefix = query.rsplit_once(' ').map_or("", |(head, _)| head).to_string();
            let predictions: Vec<String> = top_words
                .into_iter()
                .map(|(word, _)| format!("\"{}\"", format!("{} {}",query_prefix, word).trim()))
                .collect();
            return predictions;
        }
        Vec::<String>::new()
    }
}

fn get_top_completions(
    predictor: &SetPredictor,
    word: &str,
    top_n: usize,
    min_freq: usize
) -> Option<Vec<(String, usize)>> {
    let mut completions: Vec<(String, usize)> = predictor.set.iter()
    .filter(|(key, &value)| key.starts_with(word) && value >= min_freq)
    .map(|(key, &value)| (key.clone(), value))
    .collect();
    completions.sort_by(|a, b| b.1.cmp(&a.1));
    completions.truncate(top_n);
    Some(completions)
}