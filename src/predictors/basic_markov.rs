use std::{collections::HashMap, fs::{self, File}};

use csv::ReaderBuilder;
use prometheus::Registry;

use crate::importers::Importer;

use super::Predictor;

pub type MarkovChain = HashMap<String, HashMap<String, usize>>;

pub struct MarkovChainPredictor {
    chain: MarkovChain,
    configuration: HashMap<String, String>,
    update_count: usize,
    total_weight: usize
}

impl MarkovChainPredictor {
    pub fn new(metrics: Option<&Registry>) -> Self
    {
        MarkovChainPredictor {
            chain: HashMap::new(),
            configuration: HashMap::new(),
            update_count: 0,
            total_weight: 0
        }
    }

    pub fn new_from_config(config: HashMap<String, impl Into<String>>, metrics: Option<&Registry>) -> Self where Self:Sized {
        let mut configuration = HashMap::new();
        for (key, value) in config {
            configuration.insert(key, value.into());
        }
        

        MarkovChainPredictor {
            chain: HashMap::new(),
            configuration,
            update_count: 0,
            total_weight: 0
        }
    }

}

impl Predictor for MarkovChainPredictor {
    
    fn update_count(&self) -> usize {
        return self.update_count;
    }

    fn update_from_importer<I: Importer>(&mut self, importer:  &mut I, count: usize) -> Result<usize,Box<dyn std::error::Error>> {let mut raw_list = String::new();
        let blocklist: Vec<&str> = match self.configuration.get("blocklist") {
            Some(list) => {
                match fs::read_to_string(list) {
                    Ok(l) => {
                        raw_list = l.clone();
                        raw_list.lines().collect()
                    },
                    _ => Vec::new()
                }
            },
            _ => Vec::new()
            
        };

        let data = importer.fetch_queries(count)?;
        let mut count = 0;
        for q in data.iter() {
            self.update_from_query(&q.query.as_str(), Some(&blocklist));
            count += 1;
        }
        Ok(count)
    }

    
    fn update_from_query(&mut self, query: &str, blocklist: Option<&Vec<&str>>) -> Result<(), Box<dyn std::error::Error>> {
        match blocklist {
            Some(b) => {
                if contains_any(query.to_lowercase().as_str(), b.clone()) {
                    return Ok(());
                }
            },
            _ => {}
        }


        let lowercase_query = query.to_lowercase();
        let words: Vec<&str> = lowercase_query.split_whitespace().collect();
        for window in words.windows(2) {
            if let [first, second] = window {
                self.chain
                    .entry(first.to_string().to_lowercase())
                    .or_default()
                    .entry(second.to_string())
                    .and_modify(|count| *count += 1)
                    .or_insert(1);
                self.total_weight += 1;
            }
        }
        
        self.update_count += 1;
        Ok(())
    }

    fn total_weight(&self) -> usize {
        self.total_weight
    }

    fn decay(&mut self) -> () {
        for (_, inner_map) in self.chain.iter_mut() {
            // Collect keys that should be removed
            let keys_to_remove: Vec<String> = inner_map.iter_mut()
                .filter_map(|(key, value)| {
                    if *value > 0 {
                        *value -= 1;
                        if *value == 0 {
                            Some(key.clone())
                        } else {
                            None
                        }
                    } else {
                        Some(key.clone())
                    }
                })
                .collect();
            
            // Remove keys with value 0
            for key in keys_to_remove {
                inner_map.remove(&key);
            }
        }
    }

    fn predict(&self, query: &str, n: usize) -> Vec<String> {
        let split_query: Vec<&str> = query.split_whitespace().collect();
        let tf_threshold_conf = self.configuration.get("term_frequency_threshold").unwrap_or(&String::from("2")).parse::<usize>().unwrap();
        let tf_threshold = std::cmp::max(tf_threshold_conf, tf_threshold_conf*((self.update_count/10000)+1));

        if let Some(top_words) = 
            get_top_following_words(
                &self.chain,
                split_query.last().unwrap_or(&""),
                n,
                tf_threshold
            )
        {
            let predictions: Vec<String> = top_words
                .into_iter()
                .map(|(word, _)| format!("\"{} {}\"", query.trim(), word.trim()))
                .collect();
            return predictions;
        }
        Vec::<String>::new()
    }
}

fn get_top_following_words(
    markov_chain: &MarkovChain,
    word: &str,
    top_n: usize,
    min_freq: usize
) -> Option<Vec<(String, usize)>> {
    let following_words = markov_chain.get(word)?;

    // Collect and sort the words by their counts in descending order
    let mut sorted_words: Vec<(String, usize)> = following_words
        .iter()
        .map(|(word, &count)| (word.clone(), count))
        .filter(|&(_, count)| count >= min_freq)
        .collect();
    sorted_words.sort_by(|a, b| b.1.cmp(&a.1));

    // Return the top N words
    Some(sorted_words.into_iter().take(top_n).collect())
}

fn contains_any(target: &str, substrings: Vec<&str>) -> bool {
    substrings.iter().any(|&substring| substring.len()>1 && target.contains(String::from(substring).to_lowercase().as_str()))
}