use std::collections::HashMap;
use prometheus::Registry;

use super::importers::Importer;

pub mod basic_set;
pub mod basic_markov;
pub mod composite;
pub mod benchmark;


pub trait Predictor {

    fn predict(&self, query: &str, n: usize) -> Vec<String>;
    fn update_from_query(&mut self, query: &str, blocklist: Option<&Vec<&str>>) -> Result<(),Box<dyn std::error::Error>>;
    fn update_from_importer<I: Importer>(&mut self, importer: &mut I, count: usize) -> Result<usize,Box<dyn std::error::Error>>;
    fn decay(&mut self) -> ();
    fn update_count(&self) -> usize;
    fn total_weight(&self) -> usize;

    // fn new(metrics: Option<&Registry>) -> Self where Self: Sized;
    // fn new_from_config(config: HashMap<String, impl Into<String>>, metrics: Option<&Registry>) -> Self where Self:Sized;
}