use std::{collections::HashMap, f32::consts::E, fs::{self, File}, time::SystemTime};

use crate::importers::Importer;

use csv::ReaderBuilder;

use prometheus::Registry;
use regex::Regex;
use regex::RegexBuilder;

use super::{basic_markov::MarkovChainPredictor, basic_set::SetPredictor, Predictor};

pub struct CompositePredictor {
    configuration: HashMap<String, String>,
    set_predictor: SetPredictor,
    markov_predictor: MarkovChainPredictor
}

impl CompositePredictor {
    pub fn new(metrics: Option<&Registry>) -> Self
    {
        CompositePredictor {
            configuration: HashMap::new(),
            set_predictor: SetPredictor::new(metrics),
            markov_predictor: MarkovChainPredictor::new(metrics)
        }
    }

    pub fn new_from_config(config: HashMap<String, impl Into<String>>, metrics: Option<&Registry>) -> Self where Self:Sized {
        let mut configuration = HashMap::new();
        for (key, value) in config {
            configuration.insert(key, value.into());
        }
        CompositePredictor {
            configuration: configuration.clone(),
            set_predictor: SetPredictor::new_from_config(configuration.clone(), metrics),
            markov_predictor: MarkovChainPredictor::new_from_config(configuration, metrics)
        }
    }
}

impl Predictor for CompositePredictor {

    fn total_weight(&self) -> usize {
        self.markov_predictor.total_weight() + self.set_predictor.total_weight()
    }

    fn update_count(&self) -> usize {
        std::cmp::min(self.markov_predictor.update_count(), self.set_predictor.update_count())
    }

    fn update_from_importer<I: Importer>(&mut self, importer:  &mut I, count: usize) -> Result<usize,Box<dyn std::error::Error>> {


        let mut raw_list = String::new();
        let blocklist: Vec<&str> = match self.configuration.get("blocklist") {
            Some(list) => {
                match fs::read_to_string(list) {
                    Ok(l) => {
                        raw_list = l.clone();
                        raw_list.lines()
                                .filter(|line| !line.trim().is_empty())
                                .collect()
                    },
                    _ => Vec::new()
                }
            },
            _ => Vec::new()
            
        };

        let escaped_blocklist: Vec<String> = blocklist.iter().map(|&s| regex::escape(s)).collect();
        let pattern = escaped_blocklist.join("|");
        let re = RegexBuilder::new(&pattern)
            .case_insensitive(true)
            .build()
            .unwrap();


        let data = importer.fetch_queries(count)?;
        println!("got {} queries from the importer.", data.len());
        let mut count = 0;
        let mut start_time = SystemTime::now();
        for q in data.iter() {
            if(!re.is_match(&q.query)) {
                self.update_from_query(&q.query.as_str(), None);
            }
            count += 1;
            // if count%100 == 0 {
            //     println!("read {} queries in {}s", count, SystemTime::now().duration_since(start_time).unwrap().as_secs())
            // }
        }
        Ok(count)
    }

    fn update_from_query(&mut self, query: &str, blocklist: Option<&Vec<&str>>) -> Result<(), Box<dyn std::error::Error>> {
        
        self.set_predictor.update_from_query(query, blocklist);
        self.markov_predictor.update_from_query(query, blocklist);
        Ok(())
    }

    fn decay(&mut self) -> () {
        self.set_predictor.decay();
        self.markov_predictor.decay();
        
    }

    fn predict(&self, query: &str, n: usize) -> Vec<String> {
        let mut prediction = self.markov_predictor.predict(query, n);
        if prediction.len() < n {
            prediction.append(&mut self.set_predictor.predict(query, n));
            prediction.truncate(n);
        }
        prediction
    }
}