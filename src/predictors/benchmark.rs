use std::{collections::HashMap, f32::consts::E, fs::{self, File}, time::SystemTime};

use crate::importers::Importer;

use csv::ReaderBuilder;

use prometheus::{Gauge, Histogram, HistogramOpts, Opts, Registry};
use regex::Regex;
use regex::RegexBuilder;

use super::{basic_markov::MarkovChainPredictor, basic_set::SetPredictor, Predictor};

pub struct BenchmarkingPredictor<P: Predictor> {
    configuration: HashMap<String, String>,
    predictor: P,
    quality_measure: Gauge,
    total_queries: u128,
    correct_queries: u128
}

impl<P:Predictor> BenchmarkingPredictor<P> {
    pub fn new(predictor: P, metrics: Option<&Registry>) -> Self
    {

        let quality_measure_opts = Opts::new("suggestible_predictor_quality_measure", "suggestible");
        let quality_measure = Gauge::with_opts(quality_measure_opts).unwrap();

        match metrics {
            Some(registry) => {
                // Register metrics
                registry.register(Box::new(quality_measure.clone())).unwrap();
            },
            None => {}
        }
        
        let total_queries = 0;
        let correct_queries = 0;

        BenchmarkingPredictor {
            configuration: HashMap::new(),
            predictor,
            quality_measure,
            total_queries,
            correct_queries
        }
    }

    pub fn new_from_config(predictor: P, config: HashMap<String, impl Into<String>>, metrics: Option<&Registry>) -> Self where Self:Sized {
        
        let quality_measure_opts = Opts::new("suggestible_predictor_quality_measure", "suggestible");
        let quality_measure = Gauge::with_opts(quality_measure_opts).unwrap();


        match metrics {
            Some(registry) => {
                // Register metrics
                registry.register(Box::new(quality_measure.clone())).unwrap();
            },
            None => {}
        }


        let mut configuration = HashMap::new();
        for (key, value) in config {
            configuration.insert(key, value.into());
        }
        
                
        let total_queries = 0;
        let correct_queries = 0;

        BenchmarkingPredictor {
            configuration: configuration.clone(),
            predictor,
            quality_measure,
            total_queries,
            correct_queries
        }
    }
}

impl<P:Predictor> Predictor for BenchmarkingPredictor<P> {
    

    fn total_weight(&self) -> usize {
        self.predictor.total_weight()
    }

    fn update_count(&self) -> usize {
        self.predictor.update_count()
    }

    fn update_from_importer<I: Importer>(&mut self, importer:  &mut I, count: usize) -> Result<usize,Box<dyn std::error::Error>> {

        let raw_list: String;
        let blocklist: Vec<&str> = match self.configuration.get("blocklist") {
            Some(list) => {
                match fs::read_to_string(list) {
                    Ok(l) => {
                        raw_list = l.clone();
                        raw_list.lines()
                                .filter(|line| !line.trim().is_empty())
                                .collect()
                    },
                    _ => Vec::new()
                }
            },
            _ => Vec::new()
            
        };


        let escaped_blocklist: Vec<String> = blocklist.iter().map(|&s| regex::escape(s)).collect();
        let pattern = escaped_blocklist.join("|");
        let re = RegexBuilder::new(&pattern)
            .case_insensitive(true)
            .build()
            .unwrap();


        let data = importer.fetch_queries(count)?;
        println!("got {} queries from the importer.", data.len());
        let mut count = 0;
        for q in data.iter() {
            if false && !re.is_match(&q.query) {
                let _ = self.update_from_query(&q.query.as_str(), None);
            }
            count += 1;
            // if count%100 == 0 {
            //     println!("read {} queries in {}s", count, SystemTime::now().duration_since(start_time).unwrap().as_secs())
            // }
        }
        Ok(count)
    }

    fn update_from_query(&mut self, query: &str, blocklist: Option<&Vec<&str>>) -> Result<(), Box<dyn std::error::Error>> {

        if query.len() > 3 {
            self.total_queries += 1;
            let trunc_query = &query.chars().take(query.len().saturating_sub(3)).collect::<String>();
            let prediction = self.predictor.predict(trunc_query, 5);
            if prediction.contains(&format!("\"{}\"",String::from(query))) {
                self.correct_queries += 1;
            }
            if(self.total_queries>5000){
                self.quality_measure.set(self.correct_queries as f64/self.total_queries as f64);
            }
            if self.total_queries > 1000000000 {
                self.total_queries /= 1000;
                self.correct_queries /= 1000;

            }
        }
        let _ = self.predictor.update_from_query(query, blocklist);
        Ok(())
    }

    fn decay(&mut self) -> () {
        self.predictor.decay();
        
    }

    fn predict(&self, query: &str, n: usize) -> Vec<String> {
        let prediction = self.predictor.predict(query, n);
        prediction
    }
}