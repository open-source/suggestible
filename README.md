<!--
SPDX-FileCopyrightText: 2024 Phil Höfer <phil@suma-ev.de>

SPDX-License-Identifier: AGPL-3.0-only
-->
# Suggestible
![Reuse compliant](reuse-compliant.svg)


## Welcome

Thanks for checking out Suggestible. Currently we have no hosted docs.
We recommend checking out out repository and then following below instructions. Otherwise you can browse the [uncompiled docs](doc/).

### Install build tools if needed

1. **Rust and Cargo**: Ensure Rust and Cargo are installed. You can install them using [rustup](https://rustup.rs/).

    ```sh
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
    ```

2. **mdBook**: Install mdBook using Cargo. MdBook is used to generate Suggestible's documentation.

    ```sh
    cargo install mdbook
    ```

### Serve documentation

If everything is installed properly you can just let mdbook serve the docs with:
    ```
    mdbook serve
    ```
