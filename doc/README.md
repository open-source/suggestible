<!--
SPDX-FileCopyrightText: 2024 Phil Höfer <phil@suma-ev.de>

SPDX-License-Identifier: AGPL-3.0-only
-->
# Introduction

Suggestible is software toolbox for creating an auto-suggestion endpoint from a search query log file.
It aims to be a complete solution including features like:
- parsing and filtering
- multiple configurable suggestion algorithms
- automatic model updates from an incoming stream of query logs
- ready-to-use OpenSearch suggest server

## About this Documentation

Documentation is structured in two parts: User documentation and Development documentation. If you just want to use the software, [the former](./user/index.md) is for you.
Furthermore, we acknowledge divio's [documentation system](https://docs.divio.com/documentation-system/), consisting of:
* learning oriented **tutorials ◆**,
* problem oriented **how-to guides ◇**,
* understanding oriented **explanations ◈** and 
* information oriented **references ❖**.

You can see which is which using the icons placed after a documents title.

## About the Software

Suggestible is written in Rust, which means it can be very resource efficient while also being very safe.
