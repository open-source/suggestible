<!--
SPDX-FileCopyrightText: 2024 Phil Höfer <phil@suma-ev.de>

SPDX-License-Identifier: AGPL-3.0-only
-->
# Glossary ❖

- **Predictor**: 
  - Algorithm used to predict a suggestion.

- **Prediction Model**: 
  - Database used to supplement a predictor. Can be trained from search query input data.

- **Markov Chain**: 
  - Stochastic prediction model describing a sequence of possible words where the probability of each word depends only on the previous word.

- **Markov Chain Predictor**: 
  - Predictor utilising a Markov chain.

