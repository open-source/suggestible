<!--
SPDX-FileCopyrightText: 2024 Phil Höfer <phil@suma-ev.de>

SPDX-License-Identifier: AGPL-3.0-only
-->
#  Filtering
This document outlines the steps and measures we take to hopefully prevent Suggestible from giving suggestions that expose unwanted qualities of the training data.

## Risks
The indentified risks for Suggestible are as follows:
1. Leakage of personal data in the training data into the precictive model
2. Reproduction of (political or other) biases from the training data
3. Suggestion of NSFW query completions for SFW prompts
4. Output of content (or links) that are prohibited to share (ex. IP law)
4. Output of irrelevant content

## Measures

### Leakage of personal data in the training data into the precictive model