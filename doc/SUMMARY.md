<!--
SPDX-FileCopyrightText: 2024 Phil Höfer <phil@suma-ev.de>

SPDX-License-Identifier: AGPL-3.0-only
-->
# Summary

[Introduction](README.md)

# User Documentation
- [Overview](./user/index.md)
- [Trying out Suggestible ◆](./user/tutorial_basic.md)
- [Building Suggestible ◇](./user/howto_build.md)
- [Using Suggestible ◇](./user/howto_basic.md)
- [Configuration Options ❖](./user/ref_config.md)
    - [Prediction Models ❖](./user/ref_predmodels.md)
- [Further Reading]()
    - [Unterstanding Markov Chains ◈](./user/exp_markov.md)
 
# Developer Documentation
- [Overview](./dev/index.md)
- [Rustdoc ❖](./dev/rustdoc/suggestible/index.html)
- [Auxillary Documentation]()
    - [Filtering ❖](./dev/ref_filtering.md)
    - [Glossary ❖](./dev/ref_glossary.md)
