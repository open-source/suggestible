<!--
SPDX-FileCopyrightText: 2024 Phil Höfer <phil@suma-ev.de>

SPDX-License-Identifier: AGPL-3.0-only
-->

# Understanding Markov Chains

**Markov Chains** are mathematical systems that undergo transitions from one state to another within a finite or countable number of possible states. Named after the Russian mathematician Andrey Markov, these chains are widely used in statistical modeling and various practical applications, such as predicting the next word in a search query.

## How Markov Chains Work

A Markov Chain follows the *Markov property*, which states that the future state depends only on the current state and not on the sequence of events that preceded it. This property is often summarized as "memorylessness."

#### Key Components

1. **States**: Distinct conditions or positions in which the system can exist. For example, in predicting the next word, each state represents a different word.
2. **Transition Probabilities**: The probabilities of moving from one state to another. These are usually represented in a *transition matrix*.

### Transition Matrix

A transition matrix \\( P \\) is a square matrix used to describe the probabilities of transitioning from one state to another. Each element \\( P_{ij} \\) represents the probability of moving from state \\( i \\) to state \\( j \\).

Let's consider two search queries: "buy cheap laptops" and "buy cheap flights." The words involved are "buy", "cheap", "laptops", and "flights". The transition matrix might look like this:

|              | Buy | Cheap | Laptops | Flights |
|--------------|-----|-------|---------|---------|
| **Buy**      | 0.0 |  1.0  |   0.0   |   0.0   |
| **Cheap**    | 0.0 |  0.0  |   0.5   |   0.5   |
| **Laptops**  | 0.0 |  0.0  |   0.0   |   0.0   |
| **Flights**  | 0.0 |  0.0  |   0.0   |   0.0   |

In this matrix:
- \\( P_{12} = 1.0 \\) means there's a 100% chance of "cheap" following "buy".
- \\( P_{23} = 0.5 \\) means there's a 50% chance of "laptops" following "cheap".
- \\( P_{24} = 0.5 \\) means there's a 50% chance of "flights" following "cheap".

### Predicting the Next Word

Let's say you're typing a search query, and the current state is "cheap". To predict the next word using a Markov Chain, we look at the row corresponding to "cheap" in the transition matrix:

| Word   | Probability |
|--------|-------------|
| Laptops | 0.5         |
| Flights | 0.5         |

The probabilities tell us the likelihood of each possible next word. Here, "laptops" and "flights" have equal probabilities of 0.5.

## Example Calculation

If we start with the words "buy cheap" and want to predict the next word, we can use the transition matrix to calculate the probabilities step-by-step:

1. **Current State**: "buy"
2. **Next State Probabilities**:
   - "cheap": 1.0

Assuming "cheap" is chosen (since it's the only option), we then look at the row for "cheap":

3. **Next State after "cheap"**:
   - "laptops": 0.5
   - "flights": 0.5

Since "laptops" and "flights" have equal probabilities, either could be the next word. Thus, "buy cheap laptops" and "buy cheap flights" are both valid predictions.

### Applications Beyond Search Queries

Markov Chains are used in various domains such as:

- **Text Generation**: Creating sentences or paragraphs by predicting the next word.
- **Financial Modeling**: Predicting stock prices and market trends.
- **Game Theory**: Modeling decisions in strategic games.


## Conclusion

Markov Chains are valuable tools for modeling systems with probabilistic transitions between states. By understanding the current state and transition probabilities, we can predict future states effectively. However, it's crucial to be aware of their limitations and biases to ensure accurate and unbiased predictions.