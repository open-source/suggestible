<!--
SPDX-FileCopyrightText: 2024 Phil Höfer <phil@suma-ev.de>

SPDX-License-Identifier: AGPL-3.0-only
-->
# Using Suggestible ◇
This how-to helps you set up Suggestible using your own data.

If you want to just try out Suggestible with some sample data at first, we recommend to try the [introductory tutorial](tutorial_basic.md) first.
If you don't have a compiled version of Suggestible yet, follow our [build how-to](howto_build.md).
## Place Training Data
To use Suggestible, you need a csv file named `data.csv` with training data to learn suggestions from. Suggestible is looking for it in whichever folder you are running Suggestible from.
Currently, Suggestible needs the sixth column of the csv to contain the actual search queries. This is subject to change.

Here is an example of what your file could look like:
```csv
"time","referer","request_time","focus","locale","query"
2024-04-01 08:35:42.000 +0200,,,web,en,what is love?
```

## Configuring Suggestible
Currently, no configuration is possible. This will change soon.

## Running Suggestible
When you run Suggestible it will look for a data.csv in your current working directory, read it and then start a web server on port 80.

It exposes an [OpenSearch](https://github.com/dewitt/opensearch/blob/master/mediawiki/Specifications/OpenSearch/Extensions/Suggestions/1.1/Draft%201.wiki) compliant suggestions api at `/?q={searchTerms}`.