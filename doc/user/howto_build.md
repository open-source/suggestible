<!--
SPDX-FileCopyrightText: 2024 Phil Höfer <phil@suma-ev.de>

SPDX-License-Identifier: AGPL-3.0-only
-->
# Building Suggestible ◇

Building Suggestible is easy. Just start by making sure all dependencies are installed on your system, then start the build using the included makefile.

## Prerequisites

1. **Rust and Cargo**: Ensure Rust and Cargo are installed. You can install them using [rustup](https://rustup.rs/).

    ```sh
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
    ```

2. **mdBook**: Install mdBook using Cargo. MdBook is used to generate Suggestible's documentation.

    ```sh
    cargo install mdbook
    ```

## Building Suggestible

1. **Navigate to the Project Directory**: Open your terminal and change to the project directory.

    ```sh
    cd /path/to/suggestible
    ```

2. **Build Suggestible**: Use make to compile the project.

    ```sh
    make release
    ```

    This will create an optimized build in the `target/release` directory.

## Using Suggestible

To see if everything went well and Suggestible runs you can try running it through Cargo with:
```sh
cargo run --release
```
For further intructions on how to use Suggestible in your production setting refer to our [usage how-to](howto_basic.md).

## Building the Documentation

To build this documentation, simply call `make` with the `doc` target:

```sh
make doc
```

This will generate the documentation in the `book` directory.