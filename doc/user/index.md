<!--
SPDX-FileCopyrightText: 2024 Phil Höfer <phil@suma-ev.de>

SPDX-License-Identifier: AGPL-3.0-only
-->
# Overview

## Reading Journeys

### Show me what Suggestible can do

1. [Trying out Suggestible ◆](tutorial_basic.md)
2. [Unterstanding Markov Chains ◈](exp_markov.md)

### I want to use Suggestible immediately

1. [Building Suggestible ◇](howto_build.md)
2. [Using Suggestible ◇](howto_basic.md)