# Use the official Rust image as the base
FROM rust:latest

# Copy the project files to the container
COPY . .

# Build the project
RUN make release
RUN apt update && apt install vim -y

# Set the command to run the application
CMD ["./target/release/suggestible"]
