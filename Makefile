# SPDX-FileCopyrightText: 2024 Phil Höfer <phil@suma-ev.de>
# SPDX-License-Identifier: AGPL-3.0-only

# Rust project name
PROJECT_NAME := Suggestible

# Directories
SRC_DIR := src
DOC_DIR := doc
TARGET_DIR := target

# Binary name
BIN_NAME := $(PROJECT_NAME)

# Binary paths
BIN_PATH := $(TARGET_DIR)/debug/$(BIN_NAME)
RELEASE_BIN_PATH := $(TARGET_DIR)/release/$(BIN_NAME)

# Compilation commands
RUSTC := rustc
CARGO := cargo

# Documentation commands
MDBOOK := mdbook

# Targets
.PHONY: all clean doc

all: $(BIN_PATH)

release: $(RELEASE_BIN_PATH)

$(BIN_PATH): $(SRC_DIR)/*.rs
	$(CARGO) build

$(RELEASE_BIN_PATH): $(SRC_DIR)/*.rs
	$(CARGO) build --release

doc:
	$(CARGO) doc --no-deps
	cp -r target/doc/* doc/dev/rustdoc
	$(MDBOOK) build

clean:
	$(CARGO) clean
	$(MDBOOK) clean
